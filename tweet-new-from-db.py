#! /usr/bin/python3

import sys
import codecs
import json
import os
from twython import Twython
import sqlite3

dbfile = "/home/rjp/data/runs.db"

conn = sqlite3.connect(dbfile)
c = conn.cursor()

# Debugging values
app = "rtwapp"
user = "zimpenfish_wf"

# LIVE values
app = "juneathon"
user = "zimpenfish"

apis = None
with open(os.getenv('HOME', '/tmp') + '/.apis', encoding="utf-8") as file:
    apis = json.load(file)

if apis == None:
    raise "No API keys loaded"

consumer_key=apis['twitter']['apps'][app]['api_key']
consumer_secret=apis['twitter']['apps'][app]['api_secret']
access_token=apis['twitter']['users'][user]['access_token']
access_secret=apis['twitter']['users'][user]['access_secret']

twitter = Twython(consumer_key, consumer_secret, access_token, access_secret)

# Fetch all the raw data we've not yet updated into the runs table
c.execute("select raw_oid, tweet_content from runs where tweet_id is null order by ts asc limit 1")

for tweet_tuple in c.fetchall():
    raw_oid, content = tweet_tuple

    result = None
    success = 1

    try:
        print("tweet: %s" % content)
        result = twitter.update_status(status=content)
    except:
        print(sys.exc_info()[0])
        success = 0

    if success:
        print(result['id_str'])
        print("Tweeted ok")
        c.execute("update runs set tweet_id=? where raw_oid=?", [result['id_str'], raw_oid])
        conn.commit()
    else:
        c.execute("update runs set tweet_id=? where raw_oid=?", ['x', raw_oid])
        conn.commit()

conn.close()
