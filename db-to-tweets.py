#! /usr/bin/python2.7

import sys
import codecs
import json
import os
import re
import feedparser
import sqlite3
import delorean

tag = 'juneathon2016'
tags = sys.argv[1:2]
if len(tags) > 0: tag = tags[0]

# We use this a lot - better to have one copy.
MYTIMEZONE = 'Europe/London'

dbfile = "/home/rjp/data/runs.db"

conn = sqlite3.connect(dbfile)
c = conn.cursor()

globalmatch = re.compile("^(.+?)(?: \((.+)\))?: (.*)")
matcher = re.compile("^Run( \((.+)\))?: Distance: (.+), Elevation Gain: (.+), Moving Time: (.+), Average Speed: (.+)")
paced = re.compile("^Run( \((.+)\))?: Distance: (.+), Elevation Gain: (.+), Moving Time: (.+), Pace: (.+)")
run_title = re.compile("\d+/365\*?( \d+/365)? ?(.+)?")

# Fetch all the raw data we've not yet updated into the runs table
c.execute("select r.oid, r.title, r.description, r.guid, r.ts, r.tag from raw_data r left join runs s on r.oid=s.raw_oid where s.raw_oid is null and r.tag=?", [tag])

for run in c.fetchall():
    (oid, title, desc, guid, ts, tag) = run[0:6]
    print("T=%s" % (title))

    if tag == None: tag = ""
    smalltag = re.sub("\d+$", "", tag)

    dateparsed = delorean.parse(ts, yearfirst=True, dayfirst=False)
    ymd8601 = dateparsed.date.isoformat()
    if dateparsed.datetime.month != 6:
        continue

    gg = globalmatch.match(desc)
    print(gg.groups())
    if gg:
        chunks = gg.group(3).split(", ")
        cl = {}
        for i in chunks:
            key, val = i.split(": ")
            cl[key] = val
        cl['Type'] = gg.group(1)
        if gg.group(2):
            cl['Location'] = gg.group(2)
        print(cl)
        extraChunks = []
        speedChunk = ""
        if 'Pace' in cl:
            speedChunk = "Pace: %s" % cl['Pace']
        if 'Average Speed' in cl:
            speedChunk = "Speed: %s" % cl['Average Speed']
        if 'Speed' in cl:
            speedChunk = "Speed: %s" % cl['Speed']
        if 'Weighted Avg Power' in cl:
            extraChunks.append("Power: %sW" % cl['Weighted Avg Power'])

        if 'Location' in cl:
            location = re.sub(r",.*$", "", cl['Location'])
        else:
            location = "UKish"
        if len(location) > 30:
            location = "%s..." % location[0:27]

        extra = ""
        pretweet = "%s %s (%s) %s%s, %s, %s %s #%s #%s" % (location, cl['Type'], ymd8601, extra, cl['Distance'], cl['Moving Time'], speedChunk, " ".join(extraChunks), tag, smalltag)
        print(">>>>> %s" % pretweet)
        if len(pretweet) > 135:
            raise "FAIL ON TWITTER LENGTH"

#    matched = matcher.match(desc)
#    runtype = "Speed"
#    if not matched:
#        matched = paced.match(desc)
#        runtype = "Pace"
#    if matched:
#        f = matched.groups()[1:]
#        extra = "|"
#        title_bits = run_title.match(title)
#        if title_bits and title_bits.lastindex > 1:
#            extra = "|%s|" % title_bits.groups()[1]
#
#        if f[0] is None:
#            location = "UKish"
#        else:
#            location = re.sub(r",.*$", "", f[0])
#
#        tweet = "%s (%s) %s Dist: %s, Time: %s, %s: %s #%s #%s" % (location, ymd8601, extra, f[1], f[3], runtype, f[4], tag, smalltag)
        tweet = pretweet
        print("TWEET=[%s]" % tweet)
        c.execute("insert or ignore into runs values (?,?,?,?,NULL,?)", [run[0], 0, ts, tweet, tag] )
conn.commit()
conn.close()
