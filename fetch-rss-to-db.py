#! /usr/bin/python2.7

import sys
import codecs
import json
import os
import re
import feedparser
import sqlite3
import delorean

tag = 'juneathon2016'
tags = sys.argv[1:2]
if len(tags) > 0: tag = tags[0]

MYTIMEZONE = "Europe/London"

dbfile = "/home/rjp/data/runs.db"

conn = sqlite3.connect(dbfile)
c = conn.cursor()

globalmatch = re.compile("^(.+?)(?: \((.+)\))?: (.*)")
matcher = re.compile("^Run( \((.+)\))?: Distance: (.+), Elevation Gain: (.+), Moving Time: (.+), Average Speed: (.+)")
paced = re.compile("^Run( \((.+)\))?: Distance: (.+), Elevation Gain: (.+), Moving Time: (.+), Pace: (.+)")

feed = feedparser.parse("http://feedmyride.net/rides/641149?cube")
for entry in feed.entries:
    print(entry)
    ts = delorean.parse(entry.published).shift(MYTIMEZONE).datetime.isoformat()

    gg = globalmatch.match(entry.description)
    print(gg.groups())
    if gg:
        chunks = gg.group(3).split(",")
        cl = {}
        for i in chunks:
            key, val = i.split(": ")
            cl[key] = val
        cl['Type'] = gg.group(1)
        cl['Location'] = gg.group(2)
        print(cl)


    c.execute("insert or ignore into raw_data values (?,?,?,?,?,?)", [entry.title, entry.description, entry.guid, entry.guid, ts, tag])
conn.commit()
conn.close()
